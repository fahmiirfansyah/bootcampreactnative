import React, { Component } from 'react';
import {  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Button, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';




export default class App extends Component {
  state={
    data:[]
  }
  componentDidMount(){
    this.fetchData()
  }
  fetchData= async()=>{
    const response=await fetch('https://masak-apa.tomorisakura.vercel.app/api/recipes-length/?limit=3')
    const json=await response.json()
    this.setState({data:json.results})
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.boxatas} >
        <Image source={{uri:'https://m.media-amazon.com/images/M/MV5BNDUwNjBkMmUtZjM2My00NmM4LTlmOWQtNWE5YTdmN2Y2MTgxXkEyXkFqcGdeQXRyYW5zY29kZS13b3JrZmxvdw@@._V1_UX477_CR0,0,477,268_AL_.jpg'}} style= {{width:98, height:22}} />
        <View style={styles.searchitem} >
        <TouchableOpacity>
        <Icon style={styles.accountitem} name='search' size={25} />
        </TouchableOpacity>
        <TouchableOpacity>
        <Icon style={styles.accountitem} name='account-circle' size={25} />
        </TouchableOpacity>
        </View>
        </View>

        <View style={styles.body}>
        <FlatList
        data={this.state.data}
        renderItem={(data)=><ListItem data={data.item}/>}
        keyExtractor={(item)=>item.id}
        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
        />
        </View>
  
        <View style={styles.tabbar}>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='home' size={25} />
        <Text style={styles.tabtittle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='whatshot' size={25} />
        <Text style={styles.tabtittle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='subscriptions' size={25} />
        <Text style={styles.tabtittle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='folder' size={25} />
        <Text style={styles.tabtittle}>Library</Text>
        </TouchableOpacity>
        </View>
      </View>
    )
  }
}

class ListItem extends React.Component {
  render(){
  let data= this.props.data;
  return(
      <View>
          <View>
              <Image source={{ uri: data.thumb }} style={{width:50, height:50,borderRadius:25}}></Image>
          </View>
          <Text numberOfLines={2} ellipsizeMode="tail">
          {data.title}
        </Text>
      </View>
  )
  }
  }
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  boxatas: {
    height: 50,
    elevation:3,
    backgroundColor: 'white',
    paddingHorizontal:15,
    flexDirection:'row',
    alignItems:'center',
    justifyContent: 'space-between'
  },
  searchitem:{
    flexDirection:'row'

  },
  accountitem:{
    marginLeft:25
  },
  tabbar:{
    backgroundColor:'white',
    height:60,
    borderTopWidth:0.5,
    borderColor:'#E5E5E5',
    flexDirection:'row',
    justifyContent:'space-around'
  },
  body:{
    flex:1
  },
  tabitem:{
  alignItems:'center',
  justifyContent:'center'
  },
  tabtittle:{
    fontSize:11,
    color:'#3c3c3c',
    paddingTop:3
  },
  
})