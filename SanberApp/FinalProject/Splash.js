import React, { useEffect } from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace("Login")
        }, 2000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1,backgroundColor: '#4A0004'}}>
            <Image source={{uri:'https://cdn.pixabay.com/photo/2018/06/12/05/13/fire-nation-3470002_960_720.png'}} style={{width:200, height:200}}></Image>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({})
