import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';




const DEVICE = Dimensions.get('window');

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:[]
    };
  }
  
  componentDidMount(){
    this.fetchData()
  }
  fetchData= async()=>{
    const response=await fetch('https://fakestoreapi.com/products?limit=40')
    const json=await response.json()
    this.setState({data:json,memory:json})
  }
  
  searchItem=(value)=>{
    const filteredItem=this.state.memory.filter(datas=>{
        let datasLowercase=
          (datas.title).toLowerCase()
          let searchTermLowerCase=value.toLowerCase()
          return datasLowercase.indexOf(searchTermLowerCase)>-1
        }) 
        this.setState({data:filteredItem})
  }
 
  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            minHeight: 50,
            width: DEVICE.width,
          }}
        >
          
          <View style={{
            backgroundColor: '#4A0004',
            flexDirection: 'row',
            justifyContent: 'space-between' 
          }}>
          <TextInput
            style={{ backgroundColor: 'white', margin: 8,height:35 ,borderRadius: 5,width:230}}
            placeholder="Search.."
            onChangeText={(value) => this.searchItem(value)}
          />
          <TouchableOpacity style={{ margin: 8}}  >
             <Icon name='md-heart' size={30} color={'#FFCCCC'} />
            </TouchableOpacity>
            <TouchableOpacity style={{ margin: 8}} >
             <Icon name='chatbox-ellipses' size={30} color={'#FFCCCC'} />
            </TouchableOpacity>
            <TouchableOpacity style={{ margin: 8}}  >
             <Icon name='cart' size={30} color={'#FFCCCC'} />
            </TouchableOpacity>
          </View>
        </View>
        
        <FlatList
        data={this.state.data}
        renderItem={(data)=>(<TouchableOpacity  onPress={() => this.props.navigation.navigate('Detail',{detail:data.item})}><ListItem 
          data={data.item}
          /></TouchableOpacity>
          )}
        keyExtractor={(item)=>item.id}
        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
        numColumns={2}
        ></FlatList>
      </View>
    );
  }
}


class ListItem extends React.Component {
  static navigationOptions={}
  render() {
    const data = this.props.data;
    return (
      <View style={styles.itemContainer} >
        <Image
          source={{ uri: data.image }}
          style={styles.itemImage}
          
        ></Image>
        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
          {data.title}
        </Text>
        <Text style={styles.itemPrice}>
          $ {data.price}
        </Text>
        
        
      </View>
    ); 
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 5,
  },
  itemImage: {
    height:100,
    width:100,
    resizeMode:"contain"
  },
  itemName: {
    fontWeight: 'bold',
    
  },
  itemPrice: {
    fontWeight: 'bold',
    color:'blue',
    fontSize: 18
  },
  itemStock: {
    fontWeight: 'bold'
  },
  itemButton: {

  },
  buttonText: {
    
  },
});
