import React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';

import Icon from 'react-native-vector-icons/Ionicons';
import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import Splash from './Splash';
import Profile from './Profile';
import Notif from './Notif';
import Detail from './Detail';


const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator()

const index = () => {
  return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName={Splash} >
          <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }}/>
          <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={TabsScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Detail' component={Detail} options={{ headerShown: false }}  />
          
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  const TabsScreen = ()=>(
      <Tabs.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused
              ? 'home'
              : 'home-outline';
          } else if (route.name === 'Notifications') {
            iconName = focused ? 'notifications' : 'notifications-outline';
          }
          else if (route.name === 'Profile') {
            iconName = focused ? 'person-sharp' : 'person-outline';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={30} color={'#800020'} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tabs.Screen name="Home" component={HomeScreen} />
      <Tabs.Screen name="Notifications" component={Notif} />
      <Tabs.Screen name="Profile" component={Profile} />
      
    </Tabs.Navigator>
  )

export default index


