import React from 'react'
import { StyleSheet, Text, View ,Button,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
const Profile = ({navigation}) => {
    return (
        <View>
        <View style={{ paddingTop:10,paddingLeft:350,flexDirection:'row'}}>
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Icon name='logout' size={30} color={'#800020'} />
            </TouchableOpacity>
        </View>
            
     <View style={styles.container}>
        
        
        <Icon name='account-circle' size={200} color={'grey'} />
        <Text style={{fontSize:25,color:'black',fontWeight: 'bold'}}>Muhammad Fahmi Irfansyah </Text>
        <Text style={{fontSize:15,paddingBottom:10,color:'#4A0004'}}>Jual Barang Bekas</Text>
    
       
      </View>
             
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flexDirection:'column',
        justifyContent:'space-around',
        paddingTop:10,
        alignItems:'center'
      },
})
