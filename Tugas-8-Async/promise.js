console.log(' ')
console.log('Soal 2-Promise baca buku------------------------------------------------------------------------------------------')
console.log(' ')

function readbookspromise(time,book){
    console.log(`saya mulai membaca ${book.name}`)
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            let sisawaktu=time-book.timespent
            if(sisawaktu>=0){
                console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisawaktu}`)
                resolve(sisawaktu)
            }
            else{
                console.log('saya sudah tidak punya waktu untuk baca')
                reject(time)
            }
        }, book.timespent)
      })
      
}
module.exports=readbookspromise